﻿using System;
namespace EmpleadosSinHerencia
{
    public class EmpleadoTemporal : Empleado
    {
        public DateTime FechaIngreso { get; set; }
        public DateTime FechaSalida { get; set; }


        public EmpleadoTemporal()
        {
        }

        public decimal Sueldo()
        {
            return 400;
        }
    }
}
